# CryptoPartyLDN guide

A short introduction to the [CryptoParty](https://www.cryptoparty.in/)
movement and, in particular, to the series of events organised in
London under the name
[CryptoPartyLDN](https://www.cryptoparty.in/london/).

On a GNU/Linux system it should be possible to generate new versions
of the booklet by editing `booklet.md` and then using the `make`
command from the command line. A recent version of
[LaTeX](https://www.latex-project.org/) is required. Some of the used
fonts might be missing on your system.

The original content of this repository is released into the public
domain (see `LICENCE.md`), copies and derivative work are welcome.

## Credits

Original design and editing work has been done by CryptoPartyLDN using
[LaTeX](https://www.latex-project.org/),
[Inkscape](https://inkscape.org/),
[ImageMagick](http://www.imagemagick.org/), and other [Free
Software](https://www.fsf.org/) tools and applications.

The graphics on the front page is derived from one of the early
patents for the [Enigma
machine](https://en.wikipedia.org/wiki/Enigma_machine). See for
instance this
[link](https://commons.wikimedia.org/wiki/File:Scherbius-1928-patent.png).

Thanks to the [Electronic Frontier Foundation](https://www.eff.org/)
for their [Surveillance Self-Defense](https://ssd.eff.org/) project
and thanks to [Tactical Tech](https://tacticaltech.org/) for [Me and
My Shadow](https://myshadow.org/) and other projects and websites they
have published.

Credits and links to original images, fonts, and other graphics
element have been added when due. If credit to the work of a third
party is missing, please contact the maintainers of this repository
and things will be amended as soon as possible.

## Contacts

CryptoPartyLDN is organised by [Big Brother
Watch](https://bigbrotherwatch.org.uk/), a UK non-profit campaigning
for privacy and civil liberties in the UK, and [Reckon
Digital](https://reckondigital.com/), a London-based software
company. We are supported by an amazing network of infosec trainers
and privacy experts.

For questions about this repository, please send us a direct message
on our Twitter or Mastodon accounts.

- https://twitter.com/cryptopartyldn
- https://mastodon.earth/@cryptopartyldn
