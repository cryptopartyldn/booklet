---
project: The definitive(?!) guide to CryptoPartyLDN
version: 1.0
date: 21 February 2019
author: CryptoPartyLDN
---

# What is a CryptoParty

The CryptoParty movement started in Australia in August 2012. At that
time, the Australian parliament had recently passed a new cybercrime
bill that regulated law enforcement agencies' investigative powers and
the data retention obligations of internet service providers. Local
digital rights and privacy campaigners were worried about the
extension of powers granted to the government and to law enforcement
agencies.

A local journalist and activist who goes by the moniker Asher Wolf had
the idea of organising an event to bring attention to the
topic. According to Wolf, the event had to be engaging, open to
everyone, and give people the opportunity to discuss digital rights
and learn about privacy enhancing technologies. It had to be fun, with
music, art, and drinks. Wolf launched her rallying call in a series of
tweets dated 22 August 2012, where the name *CryptoParty* (and the
corresponding Twitter hashtag) appeared for the first time. These
tweets would mark the outset of a new worldwide grassroot movement.

> I want a HUGE Melbourne crypto party! BYO devices, beer, &
> music. Let's set a time and place :) Who's in? - *Asher Wolf*

# CryptoPartyLDN

In September 2017, a series of monthly CryptoParties were started in
London under the name CryptoPartyLDN.

The events are hosted at Juju's Bar & Stage in Shoreditch, an ideal
space where we combine talks, workshops, music, video art exhibitions,
and amazing crypto-themed cocktails...

CryptoPartyLDN is made possible by a network of technology experts who
regularly attend the events as trainers or speakers. The average
turnout is from 60 to 100 participants. The events are always free and
open to everyone. No registration is required.

# Who we are & why we do this

CryptoPartyLDN is organised by Big Brother Watch, a UK non-profit
campaigning for privacy and civil liberties in the UK, and Reckon
Digital, a London-based software company. We are supported by an
amazing network of infosec trainers and privacy experts.

The right to a private life is a fundamental human right that is
critical for our enjoyment of wider rights and civil liberties, and
for maintaining a healthy balance between the citizen and the
state. Only when people have privacy can they truly enjoy their
freedoms, explore and develop their personalities, politics, interests
and friendships.

# What you can ask us about

If you care about privacy and want to know more, there are a few great
online resources you can refer to, such as *Surveillance Self-Defense*
by the Electronic Frontier Foundation and *My Shadow* from Tactical
Tech.

If we had to pick the most important recommendations to improve your
security and privacy, those would probably be as follows.

\begin{resourcelist}
  \item[] \url{https://ssd.eff.org}
  \item[] \url{https://myshadow.org}
\end{resourcelist}

# General recommendations

When it comes to digital security, there's no one-size-fits-all
solution. If you have doubts or need help, get in touch or come to one
of our CryptoParties.

0. Use the free messaging app \emph{Signal} for private messaging,
  calls and video calls.
0. Keep your software up-to-date. Software vendors regularly release
  security patches as new vulnerabilities are found in their products.
0. Regularly back up your data.
0. Make sure disk encryption is enabled on your computer drives and
  your smartphone storage. This will protect your data if your device
  is stolen (if the device is turned off, ask us why and how this
  works).
0. Use strong passwords. Do not reuse the same password across
  different services. Make your life easier using a password manager.
0. Install a free and open source adblocker such as *uBlock Origin*.
0. Regularly revise your social network privacy settings to make sure
  you share the right information with the right contacts.

# Internet browsing and Tor

For general internet browsing, install an updated version of *Firefox*
or *Chrome* (or its free and open source version, *Chromium*) with
only a restricted selection of security and privacy addons such as
*NoScript*, *HTTPS Everywhere*, *uBlock Origin*, and *Privacy Badger*.

For additional security and privacy you may want to use the *Tor
Browser*. Tor anonymises your web browsing by encrypting your traffic
in multiple layers and routing it via multiple hops to
destination. Tor protects your privacy and defends you against
censorship.

\begin{resourcelist}
    \item[] \url{https://github.com/gorhill/uBlock}
    \item[] \url{https://www.eff.org/privacybadger}
    \item[] \url{https://www.eff.org/https-everywhere}
    \item[] \url{https://noscript.net}
    \item[] \url{https://torproject.org}
\end{resourcelist}

# Encrypted communications

When it comes to messaging applications we recommend *Signal* (over
Whatsapp and Telegram, ask us why) - a free app for Android and iOS
much like WhatsApp that allows you to exchange encrypted instant
messages, calls, attachments, group chats and video calls.

For email encryption use *Gnupg* + *Thunderbird* + *Enigmail*; ask us
how to securely configure Enigmail - or refer to the EFF or Tactical
Tech tutorials.

*SecureDrop* is a whistleblower submission system that media
organizations can install to securely accept documents from anonymous
sources.

\begin{resourcelist}
    \item[] \url{https://signal.org}
    \item[] \url{https://www.mozilla.org/en-US/thunderbird}
    \item[] \url{https://gnupg.org}
    \item[] \url{https://www.enigmail.net}
    \item[] \url{https://securedrop.org}
\end{resourcelist}

# Passwords

Long strings or phrases that are memorable to you make for better
passphrases than short, highly complex ones. The *diceware* method
(i.e. using a random generator) may be used to create strong
passwords. You can do this with free simple password software (a
*password manager*) such as *KeePassXC*.

Avoid reusing your passwords across multiple accounts. Where possible,
enable *two-factor* or *multi-factor authentication* (2FA or MFA).

\begin{resourcelist}
    \item[] \url{https://keepassxc.org}
    \item[] \url{https://ssd.eff.org/en/module/creating-strong-passwords}
    \item[] \url{https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess}
    \item[] \url{https://twofactorauth.org}
\end{resourcelist}

# Operating Systems

First and foremost, regularly install security updates; regularly back
up your data; make sure disk encryption is enabled. When security is
critical, you may want to reserve a separate device for a certain task
(*compartmentalisation*).

If you are willing to consider a different operating system, try a
*GNU/Linux distribution* as an alternative to Mac OS X and
Windows. Most GNU/Linux distributions are free and open source.

When extra security and privacy are needed, consider *Tails* (a free,
anonymising operating system that runs all traffic through the Tor
network) or *Qubes*.

\begin{resourcelist}
    \item[] \url{https://tails.boum.org}
    \item[] \url{https://www.qubes-os.org}
\end{resourcelist}

# Free Software

It's not just a matter of price - *free software* is free as in
*freedom*. It's software with source code that is publicly available.

If free software were cake, it would be cake that comes with a list of
ingredients and the recipe.

Free software is released under specific licences that respect users'
freedom, particularly their right to know how the software was
made. Free Software can be audited, modified and redistributed by the
community. It's software the user can control, rather than being
controlled by it.

The privacy software we recommend at CryptoPartyLDN is free software.

\begin{resourcelist}
    \item[] \url{https://www.fsf.org}
\end{resourcelist}

\newpage

\null
\vfill

\urlfont\LARGE\color{gray!80}
Contact

\normalfont\normalsize\color{black}
Big Brother Watch\
Silkie Carlo - silkie.carlo@bigbrotherwatch.org.uk\
GPG `D789 AE78 6EE4 AD37 9079 551F 8C53 94E5 C29C 45AF`\
\
Reckon Digital\
Fabio Natali - f.natali@reckondigital.com\
GPG `6E0A 4E90 3241 34F0 2788 445D 044A 25F5 267D C236`
