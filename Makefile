booklet.pdf: booklet.tex assets/*
	xelatex booklet.tex

booklet.tex: booklet.md template.tex
	pandoc --pdf-engine=xelatex booklet.md -o booklet.tex --template template.tex

clean:
	rm -fr booklet.aux booklet.log booklet.toc booklet.bbl booklet.bcf \
	       booklet.blg booklet.out booklet.run.xml booklet.tex *~
