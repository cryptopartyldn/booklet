# Licence

Original graphic elements and text of this booklet are released into
the public domain. In the words of the [Creative
Commons](https://creativecommons.org) CC0 licence, to the extent
possible under law, CryptoPartyLDN has waived all copyright and
related or neighbouring rights to this work.
